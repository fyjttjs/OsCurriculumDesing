package com.model;

import java.util.ArrayList;
import java.util.List;

public class Node<T> {
	private T data;//T泛型，可放任意数据，这里用来放名字
	private int parent;//父节点ID
	private int myself;//本节点ID
	private int type;//0为文件夹，1为文件
	private int start=0;//起始盘块号
	private List<Node<T>> childNodes;//子节点列表

	public Node() {

	}

	public Node(T data) {
		this.data = data;
		this.childNodes = new ArrayList<Node<T>>();
	}

	public Node(T data, int parent,int myself,int type) {
		this.myself = myself;
		this.data = data;
		this.parent = parent;
		this.setType(type);
		this.childNodes = new ArrayList<Node<T>>();
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public int getParent() {
		return parent;
	}

	public void setParent(int parent) {
		this.parent = parent;
	}

	public int getMyself() {
		return myself;
	}

	public void setMyself(int myself) {
		this.myself = myself;
	}

	public List<Node<T>> getChildNodes() {
		return childNodes;
	}
	
	public void setChild(Node<T> child){
		this.childNodes.add(child);
	}

	public void setChildNodes(List<Node<T>> childNodes) {
		this.childNodes = childNodes;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
	
	public void setStart(int start){
//		System.out.println(start);
		this.start=start;
	}
	
	public int getStart() {
		return start;
	}

}
