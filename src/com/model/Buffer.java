package com.model;

public class Buffer {
	private String path;//文件或文件夹路径信息
	private String fileType;//文件属性
	private int start;//起始盘块号
	private int fileLength;//文件大小
	private String OType;//操作方式
	private int readPoint;//读指针位置
	private int writePoint;//写指针位置
	
	public Buffer(){
		
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getFileLength() {
		return fileLength;
	}

	public void setFileLength(int fileLength) {
		this.fileLength = fileLength;
	}

	public String getOType() {
		return OType;
	}

	public void setOType(String oType) {
		OType = oType;
	}

	public int getReadPoint() {
		return readPoint;
	}

	public void setReadPoint(int readPoint) {
		this.readPoint = readPoint;
	}

	public int getWritePoint() {
		return writePoint;
	}

	public void setWritePoint(int writePoint) {
		this.writePoint = writePoint;
	}

}
