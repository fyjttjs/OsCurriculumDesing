package com.model;

import java.util.List;

public class Tree<T> {
	private int size;//数组长度  ，比节点个数多2
    private int count;//记录树有多少个节点  
    private List<Node<T>> nodes;//节点列表
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public int getCount() {
		return count;
	}
	public List<Node<T>> getNodes() {
		return nodes;
	}
	public void setNodes(List<Node<T>> nodes) {
		this.nodes = nodes;
	}
	public void setCount(int count) {
		this.count=count;
		
	}
	
	

}
