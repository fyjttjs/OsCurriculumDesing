package com.control;

public class PublicControl {
	
	public static String getFileType(String name){
		if(name.contains(".")){
			String[] type = name.split("\\.");
			return type[1];
		}else{
			return null;
		}
		
	}

}
