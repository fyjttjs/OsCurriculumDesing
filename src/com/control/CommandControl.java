package com.control;

import com.alibaba.fastjson.JSONObject;
import com.model.Tree;
import com.view.optionpanel.ShowMessage;

public class CommandControl {

	private MyTreeControl<String> checkTree;

	public CommandControl() {
		checkTree = new MyTreeControl<>(JSONObject.parseObject(
				TreeIOControl.getTreeIO(), Tree.class));
	}

	// 切换目录
	public String cdCommand(String path, String cm) {
		String[] cms = cm.split(" ");
		String newPath = null;
		if (cms.length >=2) {
			if (cms.length == 2) {// 相对路径
				if (checkTree.getIdByPath(path + "\\" + cms[1]) >= 0) {
					newPath = path + "\\" + cms[1];
					
				} else {
					newPath=path;
					ShowMessage.error("错误路径", null);
				}
			} else if (cms.length == 3) {// 绝对路径
				if (cms[1].equals("-a")) {
					if (checkTree.getIdByPath(cms[2]) >= 0) {
						newPath = cms[2];
					} else {
						newPath=path;
						ShowMessage.error("错误路径", null);
					}
				} else {
					newPath=path;
					ShowMessage.error("错误标识符", null);
				}
			}
			return newPath;
		}else{
			ShowMessage.error("缺少路径", null);
			return path;
		}
		
	}

	// 删除文件
	public String rmCommand(String path, String cm) {
		String[] cms = cm.split(" ");
		String newPath = null;
		if (cms.length >=2) {
			if (cms.length == 2) {// 相对路径
				if (checkTree.getIdByPath(path + "\\" + cms[1]) >= 0) {
					newPath = path + "\\" + cms[1];
				} else {
					newPath=path;
					ShowMessage.error("错误路径", null);
				}
			} else if (cms.length == 3) {// 绝对路径
				if (cms[1].equals("-a")) {
					if (checkTree.getIdByPath(cms[2]) >= 0) {
						newPath = cms[2];
					} else {
						newPath=path;
						ShowMessage.error("错误路径", null);
					}
				} else {
					newPath=path;
					ShowMessage.error("错误标识符", null);
				}
			}
			return newPath;
		}else{
			ShowMessage.error("缺少路径", null);
			return path;
		}
		
	}

}
