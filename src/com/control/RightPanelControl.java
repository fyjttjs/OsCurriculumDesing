package com.control;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Observable;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.model.Buffer;
import com.model.Node;
import com.model.Tree;
import com.view.ModifiedFlowLayout;
import com.view.NewCatalog;
import com.view.ReTypeFileDialog;
import com.view.Read;
import com.view.Write;
import com.view.XianshiDialog;
import com.view.XiugaiDialog;
import com.view.optionpanel.ShowMessage;

public class RightPanelControl extends Observable implements ActionListener, MouseListener {

	private JPanel rJPanel;
	private MyTreeControl<String> systemTree;
	private JMenuItem oneItem, oneItem1, oneItem2, oneItem3, oneItem4, twoItem, threeItem;
	private JButton icon;
	private String path, dian;

	public RightPanelControl(String path) {
		this.path = path;
		rJPanel = new JPanel();
		rJPanel.setSize(400, 500);
		rJPanel.setLayout(new ModifiedFlowLayout(FlowLayout.LEFT, 3, 3));
		systemTree = new MyTreeControl<>(JSONObject.parseObject(TreeIOControl.getTreeIO(), Tree.class));
		// System.out.println("右边面板获取的路径是"+this.path);
		List<Node<String>> nodes = systemTree.getNode(systemTree.getIdByPath(path)).getChildNodes();
		for (Node<String> n : nodes) {
			ImageIcon picture;
			if (n.getType() == 0) {
				picture = new ImageIcon("picture/maomi.png");
			}

			else {
				picture = new ImageIcon("picture/yu.png");
			}

			icon = new JButton(n.getData(), picture);
			icon.setIconTextGap(2);
			icon.setHorizontalTextPosition(JButton.CENTER);
			icon.setVerticalTextPosition(JButton.BOTTOM);
			icon.setBorderPainted(false);
			icon.addActionListener(this);
			icon.addMouseListener(this);
			rJPanel.add(icon);

		}
	}

	public JPanel getRightPanel() {
		return this.rJPanel;
	}

	public JPopupMenu getJPopupMenu() {
		JPopupMenu tanchu = new JPopupMenu();
		oneItem = new JMenuItem("读");
		oneItem.addActionListener(this);
		oneItem1 = new JMenuItem("写");
		oneItem1.addActionListener(this);
		oneItem2 = new JMenuItem("显示内容");
		oneItem2.addActionListener(this);
		oneItem3 = new JMenuItem("修改内容");
		oneItem3.addActionListener(this);
		oneItem4 = new JMenuItem("修改属性");
		oneItem4.addActionListener(this);
		twoItem = new JMenuItem("删除");
		twoItem.addActionListener(this);
		threeItem = new JMenuItem("详细信息");
		threeItem.addActionListener(this);
		tanchu.add(oneItem);
		tanchu.add(oneItem1);
		tanchu.add(oneItem2);
		tanchu.add(oneItem3);
		tanchu.add(oneItem4);
		tanchu.add(threeItem);
		tanchu.add(twoItem);

		return tanchu;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		BufferControl bufferControl = BufferControl.getInstance();
		if (e.getSource() instanceof JButton) {
			String newpath = this.path + "\\" + e.getActionCommand().toString();
			if (systemTree.getNode(systemTree.getIdByPath(newpath)).getType() == 0) {
				super.setChanged();
				this.notifyObservers(this.path + "\\" + e.getActionCommand().toString());
			} else {
				Buffer buffer = null;
				try {
					buffer = bufferControl.newBuffer(newpath, "显示内容",
							systemTree.getNode(systemTree.getIdByPath(newpath)), -1);
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				if (bufferControl.addBuffer(buffer)) {
					int id = systemTree.getIdByPath(newpath);
					
					if(buffer.getPath().equals("0000")){
						bufferControl.removeBuffer(buffer);
					}
					else {
						new XianshiDialog(id, buffer);
					}
				}

			}
		} 
		else if (e.getSource() == twoItem) {//删除
			
			List<Buffer> buffers=BufferControl.getInstance().getBuffers();
			int i=buffers.size();
			int j=0;
			for(;j<i;j++){
				Buffer y=buffers.get(j);
				if(y.getPath().equals(path + "\\" + this.dian)){
					j=-1;
						ShowMessage.error("该文件已打开，请在已打开文件表中关闭再删除！", null);
						break;
					
				}
			}
			if(j!=-1){
			super.setChanged();
			this.notifyObservers("删除文件\\" + this.dian);
			}
		} 
		else if (e.getSource() == oneItem) {//读
			if (systemTree.getNode(systemTree.getIdByPath(path + "\\" + this.dian)).getType() == 1) {

				if (PublicControl.getFileType(this.dian).equals("只读")) {
					if (bufferControl.getBuffers().size() < 5) {
						
						List<Buffer> buffers=BufferControl.getInstance().getBuffers();
						int i=buffers.size();
						int j=0;
						for(;j<i;j++){
							Buffer y=buffers.get(j);
							if(y.getPath().equals(path + "\\" + this.dian)){
								if(y.getOType().equals("读")){
									
									break;
								}
								else {
									j=-1;
									ShowMessage.error("该文件已以其他形式打开，请在已打开文件表中关闭再打开！", null);
									break;
								}
							}
						}
						
						if(j!=-1){
							System.out.println("3"+j);
						Read read = new Read(systemTree.getIdByPath(path + "\\" + this.dian));
						Buffer buffer = null;
						try {
							buffer = bufferControl.newBuffer(path + "\\" + this.dian, e.getActionCommand().toString(),
									systemTree.getNode(systemTree.getIdByPath(path + "\\" + this.dian)),
									read.getReadPoint());
						} catch (FileNotFoundException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						if(buffer.getPath().equals("0000")){
							
						}
						else {
							bufferControl.addBuffer(buffer);
						}
					}
						
					} else {
						ShowMessage.error("运行内存已满！", null);
					}
						
				} else if (PublicControl.getFileType(this.dian).equals("可读写")) {
					if (bufferControl.getBuffers().size() < 5) {
						
						List<Buffer> buffers=BufferControl.getInstance().getBuffers();
						int i=buffers.size();
						int j=0;
						for(;j<i;j++){
							Buffer y=buffers.get(j);
							if(y.getPath().equals(path + "\\" + this.dian)){
								if(y.getOType().equals("读")){
									
									break;
								}
								else {
									j=-1;
									ShowMessage.error("该文件已以其他形式打开，请在已打开文件表中关闭再打开！", null);
									break;
								}
							}
						}
						if(j!=-1){
						Read read = new Read(systemTree.getIdByPath(path + "\\" + this.dian));
						Buffer buffer = null;
						try {
							buffer = bufferControl.newBuffer(path + "\\" + this.dian, e.getActionCommand().toString(),
									systemTree.getNode(systemTree.getIdByPath(path + "\\" + this.dian)),
									read.getReadPoint());
						} catch (FileNotFoundException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						if(buffer.getPath().equals("0000")){
							
						}
						else {
							bufferControl.addBuffer(buffer);
						}
					}
					} else {
						ShowMessage.error("运行内存已满！", null);
					}
				} else {
					ShowMessage.warning("系统文件无法执行读操作！", null);
				}
			} else {
				ShowMessage.warning("文件夹无法执行读操作！", null);
			}
			super.setChanged();
			this.notifyObservers(this.path);
		} else if (e.getSource() == oneItem1) {
			if (systemTree.getNode(systemTree.getIdByPath(path + "\\" + this.dian)).getType() == 1) {
				System.out.println(PublicControl.getFileType(this.dian));
				if (PublicControl.getFileType(this.dian).equals("可读写")) {
					if (bufferControl.getBuffers().size() < 5) {
						
						List<Buffer> buffers=BufferControl.getInstance().getBuffers();
						int i=buffers.size();
						int j=0;
						for(;j<i;j++){
							Buffer y=buffers.get(j);
							if(y.getPath().equals(path + "\\" + this.dian)){
								if(y.getOType().equals("写")){
									
									break;
								}
								else {
									j=-1;	
									ShowMessage.error("该文件已以其他形式打开，请在已打开文件表中关闭再打开！", null);
									break;
								}
							}
						}
						if(j!=-1){
						Write write = new Write(systemTree.getIdByPath(path + "\\" + this.dian),systemTree.getNode(systemTree.getIdByPath(path + "\\" + this.dian)).getStart());
						Buffer buffer = null;
						try {
							buffer = bufferControl.newBuffer(path + "\\" + this.dian, e.getActionCommand().toString(),
									systemTree.getNode(systemTree.getIdByPath(path + "\\" + this.dian)),
									write.getWritePoint());
						} catch (FileNotFoundException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						if(buffer.getPath().equals("0000")){
							
						}
						else {
							bufferControl.addBuffer(buffer);
						}
						}
					} else {
						ShowMessage.error("运行内存已满！", null);
					}
				} else if (PublicControl.getFileType(this.dian).equals("只读")) {
					ShowMessage.warning("只读文件无法执行写操作！", null);
				} else if (PublicControl.getFileType(this.dian).equals("系统")) {
					ShowMessage.warning("系统文件无法执行写操作！", null);
				}
			} else {
				ShowMessage.warning("文件夹无法执行写操作！", null);
			}
			super.setChanged();
			this.notifyObservers(this.path);
		} else if (e.getSource() == threeItem) {
			if (systemTree.getNode(systemTree.getIdByPath(path + "\\" + this.dian)).getType() == 1) {
				if (bufferControl.getBuffers().size() < 5) {
					
					Buffer buffer = null;
					try {
						buffer = bufferControl.newBuffer(path + "\\" + this.dian, e.getActionCommand().toString(),
								systemTree.getNode(systemTree.getIdByPath(path + "\\" + this.dian)), -1);
					} catch (FileNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					if(buffer.getPath().equals("0000")){
						
					}
					else {
						new NewCatalog(systemTree.getIdByPath(path + "\\" + this.dian), path + "\\" + this.dian,
								PublicControl.getFileType(this.dian),
								systemTree.getNode(systemTree.getIdByPath(path + "\\" + this.dian)).getStart());
						bufferControl.addBuffer(buffer);
					}
				} else {
					ShowMessage.error("运行内存已满！", null);
				}
			} else {
				new NewCatalog(systemTree.getIdByPath(path + "\\" + this.dian), path + "\\" + this.dian, "文件夹",
						systemTree.getNode(systemTree.getIdByPath(path + "\\" + this.dian)).getStart());

			}

		} else if (e.getSource() == oneItem2) {
			if (systemTree.getNode(systemTree.getIdByPath(path + "\\" + this.dian)).getType() == 1) {
				Buffer buffer = null;
				try {
					buffer = bufferControl.newBuffer(path + "\\" + this.dian, e.getActionCommand().toString(),
							systemTree.getNode(systemTree.getIdByPath(path + "\\" + this.dian)), -1);
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				if (bufferControl.addBuffer(buffer)) {
					
					if(buffer.getPath().equals("0000")){
						bufferControl.removeBuffer(buffer);
					}
					else {
						new XianshiDialog(systemTree.getIdByPath(path + "\\" + this.dian), buffer);

					}
									}
			} else {
				ShowMessage.warning("文件夹无法执行显示操作！", null);
			}
		} else if (e.getSource() == oneItem3) {//修改内容
			if (systemTree.getNode(systemTree.getIdByPath(path + "\\" + this.dian)).getType() == 1) {
				if (bufferControl.getBuffers().size() < 5) {
					
					List<Buffer> buffers=BufferControl.getInstance().getBuffers();
					int i=buffers.size();
					int j=0;
					for(;j<i;j++){
						Buffer y=buffers.get(j);
						if(y.getPath().equals(path + "\\" + this.dian)){
							if(y.getOType().equals("修改内容")){
								
								break;
							}
							else {
								j=-1;
								ShowMessage.error("该文件已以其他形式打开，请在已打开文件表中关闭再打开！", null);
								break;
							}
						}
					}
					if(j!=-1){
					new XiugaiDialog(systemTree.getIdByPath(path + "\\" + this.dian),
							systemTree.getNode(systemTree.getIdByPath(path + "\\" + this.dian)).getStart());
					Buffer buffer = null;
					try {
						buffer = bufferControl.newBuffer(path + "\\" + this.dian, e.getActionCommand().toString(),
								systemTree.getNode(systemTree.getIdByPath(path + "\\" + this.dian)), -1);
					} catch (FileNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					if(buffer.getPath().equals("0000")){
						
					}
					else {
						bufferControl.addBuffer(buffer);
					}
					}
				} else {
					ShowMessage.error("运行内存已满！", null);
				}
			} else {
				ShowMessage.warning("文件夹无法执行修改操作！", null);
			}
		} else if (e.getSource() == oneItem4) {
			if (systemTree.getNode(systemTree.getIdByPath(path + "\\" + this.dian)).getType() == 1) {
				if (bufferControl.getBuffers().size() < 5) {
					
					List<Buffer> buffers=BufferControl.getInstance().getBuffers();
					int i=buffers.size();
					int j=0;
					for(;j<i;j++){
						Buffer y=buffers.get(j);
						if(y.getPath().equals(path + "\\" + this.dian)){
							if(y.getOType().equals("修改属性")){
								
								break;
							}
							else {
								j=-1;
								ShowMessage.error("该文件已以其他形式打开，请在已打开文件表中关闭再打开！", null);
								break;
							}
						}
					}
					
					if(j!=-1){
					Node<String> node = systemTree.getNode(systemTree.getIdByPath(this.path + "\\" + this.dian));
					List<Node<String>> list = systemTree.getNode(node.getParent()).getChildNodes();
					String newNameString = null;
					ReTypeFileDialog rtfd = new ReTypeFileDialog(node.getData());
					System.out.println("hahaha");
					newNameString = rtfd.getName();
					boolean boo = true;
					for (Node<String> n : list) {
						if (n.getData().toString().equals(newNameString))
							boo = false;
					}
					if (boo) {
						node.setData(newNameString);
						systemTree.updateNode(node);
						String newTree = getTreeJSon(this.systemTree);
						TreeIOControl.setTreeIO(newTree);
						super.setChanged();
						this.notifyObservers(this.path);
					} else {
						super.setChanged();
						this.notifyObservers(this.path);
						ShowMessage.error("同目录下文件不能重名", null);
					}

					Buffer buffer = null;
					try {
						List<Buffer> buffers1=BufferControl.getInstance().getBuffers();
						int i1=buffers.size();
						int j1=0;
						for(;j1<i1;j1++){
							Buffer y=buffers1.get(j1);
							if(y.getPath().equals(path + "\\" + this.dian)){
								bufferControl.removeBuffer(y);
							}
						}
						buffer = bufferControl.newBuffer(path + "\\" + newNameString, e.getActionCommand().toString(),
								systemTree.getNode(systemTree.getIdByPath(path + "\\" + newNameString)), -1);
					} catch (FileNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					if(buffer.getPath().equals("0000")){
						
					}
					else {
						bufferControl.addBuffer(buffer);
					}
					}
				} else {
					ShowMessage.error("运行内存已满！", null);
				}
			} else {
				ShowMessage.warning("文件夹无法执行修改操作！", null);
			}
		}
	}

	public String getTreeJSon(MyTreeControl<String> mytree) {
		return JSON.toJSONString(mytree.getTree());

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		int c = e.getButton();
		if (c == MouseEvent.BUTTON3) {
			JButton button = (JButton) e.getSource();
			this.dian = ((JButton) e.getSource()).getActionCommand();
			// this.path=this.path+"\\"+((JButton)e.getSource()).getActionCommand();
			getJPopupMenu().show(button, e.getX(), e.getY());
		}

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {

	}

}
