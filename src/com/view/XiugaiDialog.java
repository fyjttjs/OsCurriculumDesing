package com.view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.Writer;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import com.control.BufferControl;
import com.model.Buffer;
import com.view.optionpanel.ShowMessage;

public class XiugaiDialog extends JDialog implements ActionListener, WindowListener{
	private static final long serialVersionUID = 1L;
	private JButton button,button2;
	int iid;
	JTextArea jta;
	int start;
	int a[]=new int [128];
	int b[]=new int[128];
	int c[]=new int[128];
	String oldString;
	String string;
	
	public XiugaiDialog(int id,int start){
		this.setTitle("修改文件内容");
		this.setSize(350, 328);
		this.iid=id;
		this.start=start;
		JPanel jPanel = getTestArea(id); 
		this.setLocationRelativeTo(null);
		this.setModal(true);
		this.add(jPanel);
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setVisible(true);
	    this.addWindowListener(this);
		
	}
	
	public JPanel getTestArea(int id){
		JPanel jp = new JPanel(new BorderLayout());
		jta = new JTextArea(13,24);
		jta.setLineWrap(true);
		//jta.setEnabled(false);
		jta.setText(getText(id));
		oldString=getText(id);
		jp.add(jta,BorderLayout.NORTH);
		
		JPanel txt1 = new JPanel(new BorderLayout());
		button = new JButton("确定哈");
		button2 = new JButton("取消咯");
		txt1.add(button, BorderLayout.WEST);
		txt1.add(button2, BorderLayout.EAST);
		jp.add(txt1,BorderLayout.SOUTH);
		button.addActionListener(this);
		button2.addActionListener(this);
		return jp;
	}
	
	public String getText(int id){
		File file = new File("resources/"+id+".txt");
		Reader reader = null;
		try {
			reader = new FileReader(file);
		} catch (FileNotFoundException e) {
			ShowMessage.error("找不到资源文件", null);
		}
		int len = 0;
		char[] c = new char[5000];
		int temp = 0;
		try {
			while ((temp = reader.read()) != -1) {
				c[len] = (char) temp;
				len++;
			}
		} catch (IOException e) {
			ShowMessage.error("读取文件失败", null);
		}
		try {
			reader.close();
		} catch (IOException e) {
			ShowMessage.error("读取文件失败", null);
		}
		return new String(c, 0, len);
	}

	@Override
	public void actionPerformed(ActionEvent a) {
		// TODO Auto-generated method stub
		if(a.getSource()==button){
			string=jta.getText();
			File f = new File("resources/"+this.iid+".txt");
			Writer rdf = null;
			try {
				rdf = new FileWriter(f);
			} catch (IOException e) {
				ShowMessage.error("创建输出流失败", null);
				this.dispose();
			}
			try {
				rdf.write(string);
			} catch (IOException e) {
				ShowMessage.error("写入文件失败", null);
				this.dispose();
			}
			try {
				rdf.close();
			} catch (IOException e) {
				ShowMessage.error("输出流关闭失败", null);
				this.dispose();
			}
			try {
				kuaihao();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.dispose();
		}
			else if (a.getActionCommand().equals("取消咯")){
				this.dispose();
			}
		}
		
		private void kuaihao() throws Exception{
			FileReader fr=new FileReader("resources/test2.txt");
	        BufferedReader br=new BufferedReader(fr);
	        String line="";
	        int i=0;
	        while ((line=br.readLine())!=null) {
	        	int d = Integer.parseInt(line);
	        	a[i]=d;a[0]=-1;
	        	b[i]=d;b[0]=-1;
	        	c[i]=d;c[0]=-1;
	        	i++;
//	            System.out.println(d);
	        }
	        br.close();
	        fr.close();
	        
	        if(this.start>0){
	        	int o=this.start;
	        	while(o!=-1){
	        		int c=a[o];
	        		if(a[o]==-1){
	        			a[o]=0;
	        			break;
	        		}
	        		else{
	        		a[o]=0;
	        		o=c;
	        		}
	        	}
	        }
	        b=a;
	        
	        int size;
	        InputStream f1 = new FileInputStream("resources/"+this.iid+".txt");
	        size = f1.available();
	        f1.close();
	        
	        int j=1;
	        for(;j<128;j++){
	        	if(a[j]==0){
	        		this.start=j;
//	        		System.out.println(j);
	        		size=size-64;
	        		break;
	        	}
	        }
	        
	        int k=j+1;
	        if(size>0){
	        	if(j<127){
	        	for(;k<128;k++){
	        		if(a[k]==0){
	        			a[j]=k;
	        			j=k;
	        			size=size-64;
	        		}
	        		 if(size<=0){
	        			 a[k]=-1;
	        			 break;
	        		 }
	        	}
	        	if(size>0){
	        		a=c;
	        		string=oldString;
	        		File f = new File("resources/"+this.iid+".txt");
	    			Writer rdf = null;
	    			try {
	    				rdf = new FileWriter(f);
	    			} catch (IOException e) {
	    				ShowMessage.error("创建输出流失败", null);
	    				this.dispose();
	    			}
	    			try {
	    				rdf.write(string);
	    			} catch (IOException e) {
	    				ShowMessage.error("写入文件失败", null);
	    				this.dispose();
	    			}
	    			try {
	    				rdf.close();
	    			} catch (IOException e) {
	    				ShowMessage.error("输出流关闭失败", null);
	    				this.dispose();
	    			}
	        		ShowMessage.warning("存储空间已满！", null);
	        	}
	        }
	        	else {
	        		a=c;
	        		string=oldString;
	        		File f = new File("resources/"+this.iid+".txt");
	    			Writer rdf = null;
	    			try {
	    				rdf = new FileWriter(f);
	    			} catch (IOException e) {
	    				ShowMessage.error("创建输出流失败", null);
	    				this.dispose();
	    			}
	    			try {
	    				rdf.write(string);
	    			} catch (IOException e) {
	    				ShowMessage.error("写入文件失败", null);
	    				this.dispose();
	    			}
	    			try {
	    				rdf.close();
	    			} catch (IOException e) {
	    				ShowMessage.error("输出流关闭失败", null);
	    				this.dispose();
	    			}
	        		ShowMessage.warning("存储空间已满！", null);
				}
	        }
	        else {
				a[j]=-1;
			}
	        
	        File q = new File("resources/test2.txt");
			Writer rdf = null;
				rdf = new FileWriter(q);
//				rdf.write("-1\n");
				rdf.close();
	        
	        FileWriter fileWriter=new FileWriter("resources/test2.txt",true);
			for (int f = 0; f < a.length; f++) {
			fileWriter.write(String.valueOf(a[f])+"\n");
			}
			fileWriter.flush();
			fileWriter.close();
		}

		@Override
		public void windowActivated(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowClosed(WindowEvent e) {
			//BufferControl.getInstance().removeBuffer(buffer);
			
		}

		@Override
		public void windowClosing(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowDeactivated(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowDeiconified(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowIconified(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowOpened(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}
		
	}

