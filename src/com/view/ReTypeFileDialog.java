package com.view;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.view.optionpanel.ShowMessage;



public class ReTypeFileDialog extends JDialog implements ActionListener{
	private static final long serialVersionUID = 1L;
	private JPanel filePanel;
	private JComboBox<String> typeNameField;
	private JTextField fileNameField;
	private JLabel fileNameLabel,typeNameJLabel;
	private JButton teamLoginButton,modifyButton;
	private String newName;
	String name;
	public ReTypeFileDialog(String name){
		this.setTitle("修改文件类型");
		this.setSize(280, 210);
		JPanel jPanel = getTeamPanel(name);
		this.setContentPane(jPanel);
		this.setLocationRelativeTo(null);
		this.setModal(true);
		this.setDefaultCloseOperation(0);
		this.setVisible(true);

	}
	
	private JPanel getTeamPanel(String name){
		filePanel = new JPanel();
		filePanel.setLayout(null);
		this.name =name;
		typeNameJLabel = new JLabel("文件类型");
		typeNameJLabel.setBounds(30, 70, 80, 30);
		typeNameField = new JComboBox<String>(new String[]{"选择文件类别",".只读",".可读写",".系统"});
		typeNameField.setBounds(110, 70, 120, 30);
		fileNameLabel = new JLabel("文件名字");
		fileNameLabel.setBounds(30, 20, 80, 30);
		fileNameField = new JTextField();
		fileNameField.setText(getName(name));
		fileNameField.setEditable(false);
		fileNameField.setBounds(110, 20, 120, 30);
		teamLoginButton = new JButton("确   定");
		teamLoginButton.addActionListener(this);
		teamLoginButton.setBounds(20, 120, 100, 30);
		modifyButton = new JButton("取  消");
		modifyButton.addActionListener(this);
		modifyButton.setBounds(140, 120, 100, 30);
		filePanel.add(fileNameLabel);
		filePanel.add(fileNameField);
		filePanel.add(typeNameJLabel);
		filePanel.add(typeNameField);
		filePanel.add(teamLoginButton);
		filePanel.add(modifyButton);
		return filePanel;			
	}
	
	public String getName(String name){
		String[] thename = name.split("\\.");
		return thename[0];
	}
	
	public String getFileType(){
		return typeNameField.getSelectedItem().toString();
	}
	
	public String getName(){
		return this.newName;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equals("确   定")){
			if(!getFileType().equals("选择文件类别")){
			if(e.getActionCommand().equals("确   定")){
				this.newName=(fileNameField.getText().toString()+getFileType());
			}
			
			dispose();
			}else{
				ShowMessage.warning("请选择文件类型", null);
			}
		}
		else {
		
				this.newName=name;
				dispose();
		}
		
	}

}
