package com.view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import com.view.optionpanel.ShowMessage;

public class NewFile extends JDialog implements ActionListener{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextArea file;
	private String name;
	private int i;
	private int a[]=new int[128],b[]=new int[128];
	private int start;
	int p=0;
	public NewFile(String name,int i) {
		this.name=name;
		this.i=i;
		this.setTitle("新建文件");
		this.setSize(300, 300);
		this.setLocation(550, 200);
		JPanel txt = new JPanel(new BorderLayout());
		JPanel txt1 = new JPanel(new BorderLayout());
		file = new JTextArea();
		file.setLineWrap(true);
		txt.add(file, BorderLayout.CENTER);
		JButton button = new JButton("确定哈");
		JButton button2 = new JButton("取消咯");
		txt1.add(button, BorderLayout.WEST);
		txt1.add(button2, BorderLayout.EAST);
		txt.add(txt1,BorderLayout.SOUTH);
		button.addActionListener(this);
		button2.addActionListener(this);
		this.setModal(true);
		this.setDefaultCloseOperation(0);
		this.add(txt);
		this.setVisible(true);
	}
	public int getStart(){
		return this.start;
	}

	@Override
	public void actionPerformed(ActionEvent a) {
		// TODO Auto-generated method stub
		if (a.getActionCommand().equals("确定哈")) {
		String string=file.getText();
		File f = new File("resources/"+this.i+".txt");
		Writer rdf = null;
		try {
			rdf = new FileWriter(f);
		} catch (IOException e) {
			ShowMessage.error("创建输出流失败", null);
			this.dispose();
		}
		try {
			rdf.write(string);
		} catch (IOException e) {
			ShowMessage.error("写入文件失败", null);
			this.dispose();
		}
		try {
			rdf.close();
		} catch (IOException e) {
			ShowMessage.error("输出流关闭失败", null);
			this.dispose();
		}
		try {
			kuaihao();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.dispose();
	}
		else if (a.getActionCommand().equals("取消咯")){
			start=-128;
			this.dispose();
		}
	}
	
	private void kuaihao() throws Exception{
		FileReader fr=new FileReader("resources/test2.txt");
        BufferedReader br=new BufferedReader(fr);
        String line="";
        int i=0;
        while ((line=br.readLine())!=null) {
        	int d = Integer.parseInt(line);
        	a[i]=d;a[0]=-1;
        	b[i]=d;b[0]=-1;
        	i++;
//            System.out.println(d);
        }
        br.close();
        fr.close();
        
        int size;
        InputStream f1 = new FileInputStream("resources/"+this.i+".txt");
        size = f1.available();
//        System.out.println("Total Available Bytes: " + size);
        f1.close();
        
        int j=1;
        for(;j<128;j++){
        	if(a[j]==0){
        		this.start=j;
//        		System.out.println(j);
        		size=size-64;
        		break;
        	}
        }
        
        int k=j+1;
        if(size>0){
        	if(j<127){
        	for(;k<128;k++){
        		if(a[k]==0){
        			a[j]=k;
        			j=k;
        			size=size-64;
        		}
        		 if(size<=0){
        			 a[k]=-1;
        			 break;
        		 }
        	}
        	if(size>0){
        		a=b;
        		start=-128;
        		ShowMessage.warning("存储空间已满！", null);
        	}
        }
        	else {
        		a=b;
        		start=-128;
        		ShowMessage.warning("存储空间已满！", null);
			}
        }
        else {
			a[j]=-1;
		}
        
        File q = new File("resources/test2.txt");
		Writer rdf = null;
			rdf = new FileWriter(q);
//			rdf.write("-1\n");
			rdf.close();
        
        FileWriter fileWriter=new FileWriter("resources/test2.txt",true);
		for (int f = 0; f < a.length; f++) {
		fileWriter.write(String.valueOf(a[f])+"\n");
		}
		fileWriter.flush();
		fileWriter.close();
	}
	
	public int get_p() {
		return p;
	}

}
