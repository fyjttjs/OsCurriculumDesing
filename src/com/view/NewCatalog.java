package com.view;

import java.awt.BorderLayout;

import java.awt.Dimension;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;


import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;




public class NewCatalog extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NewCatalog(int id,String lj,String sx,int kh){
		this.setSize(815, 90);
		this.setLocation(325, 250);
		JPanel jPanel = new JPanel();
		int size = 0;
		if(sx.equals("文件夹")){
			size=8;
		}
		else {
			
        InputStream f1 = null;
		try {
			f1 = new FileInputStream("resources/"+id+".txt");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        try {
			size = f1.available();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        try {
			f1.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
		String[] columnNames={"ID","文件路径名","文件属性","起始盘块号","文件长度（B）"};
		  Object[][]data={
		  {id,lj,sx,kh,size}
		  };
		  
		  
		 JTable table=new JTable(data,columnNames);
		 
		 table.getColumnModel().getColumn(0).setPreferredWidth(5);
		 table.getColumnModel().getColumn(1).setPreferredWidth(400);
		 table.getColumnModel().getColumn(2).setPreferredWidth(60);
		 table.getColumnModel().getColumn(3).setPreferredWidth(60);
		 table.getColumnModel().getColumn(3).setPreferredWidth(60);
		 table.setShowHorizontalLines(isDisplayable());
		 table.setPreferredScrollableViewportSize(new Dimension(800,24));
		 
		 DefaultTableCellRenderer tcr = new DefaultTableCellRenderer();//单元格渲染器  
		 tcr.setHorizontalAlignment(JLabel.CENTER);//居中显示  
		 table.setDefaultRenderer(Object.class, tcr);//设置渲染器  
		 
		 
		 JScrollPane scrollPane=new JScrollPane(table);
		 getContentPane().add(scrollPane,BorderLayout.CENTER);
		 jPanel.add(scrollPane);
		 
		    this.add(jPanel);
			this.setTitle("详细信息");
			this.setVisible(true);
		 }
	}


