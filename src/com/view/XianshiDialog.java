package com.view;



import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import com.control.BufferControl;
import com.model.Buffer;
import com.view.optionpanel.ShowMessage;


public class XianshiDialog extends JFrame implements WindowListener{
	private static final long serialVersionUID = 1L;
	private Buffer buffer;

	public XianshiDialog(int id,Buffer buffer){
		this.setTitle("显示文件内容");
		this.setSize(350, 300);
		JPanel jPanel = getTestArea(id); 
		this.setLocationRelativeTo(null);
		this.add(jPanel);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setVisible(true);
		this.buffer=buffer;
		this.addWindowListener(this);
		//System.out.println("显示"+id);
//		
//		 try {
//             String encoding="GBK";
//             File file=new File("resources/"+id+".txt");
//             if(file.isFile() && file.exists()){ //判断文件是否存在
//                 InputStreamReader read = new InputStreamReader(
//                 new FileInputStream(file),encoding);//考虑到编码格式
//                 BufferedReader bufferedReader = new BufferedReader(read);
//                 String lineTxt = null;
//                 while((lineTxt = bufferedReader.readLine()) != null){
//                     System.out.println(lineTxt);
//                 }
//                 read.close();
//     }else{
//    	 ShowMessage.error("找不到指定的文件",null);
//     }
//     } catch (Exception e) {
//    	 e.printStackTrace();
//    	 ShowMessage.error("读取文件内容出错",null);
//         
//     }
//		
	}
	
	
	public JPanel getTestArea(int id){
		JPanel jp = new JPanel();
		JTextArea jta = new JTextArea(13,24);
		jta.setLineWrap(true);
		jta.setEnabled(false);
		jta.setText(getText(id));
		jp.add(jta);
		return jp;
	}
	
	public String getText(int id){
		File file = new File("resources/"+id+".txt");
		Reader reader = null;
		try {
			reader = new FileReader(file);
		} catch (FileNotFoundException e) {
			ShowMessage.error("找不到资源文件", null);
		}
		int len = 0;
		char[] c = new char[5000];
		int temp = 0;
		try {
			while ((temp = reader.read()) != -1) {
				c[len] = (char) temp;
				len++;
			}
		} catch (IOException e) {
			ShowMessage.error("读取目录文件失败", null);
		}
		try {
			reader.close();
		} catch (IOException e) {
			ShowMessage.error("读取目录文件失败", null);
		}
		return new String(c, 0, len);
	}
	
	@Override
	public void windowActivated(WindowEvent arg0) {
		
	}

	@Override
	public void windowClosed(WindowEvent arg0) {
		//BufferControl.getInstance().removeBuffer(this.buffer);
		
	}

	@Override
	public void windowClosing(WindowEvent arg0) {

		
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {

		
	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {

		
	}

	@Override
	public void windowIconified(WindowEvent arg0) {

		
	}

	@Override
	public void windowOpened(WindowEvent arg0) {

		
	}

}
