package com.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.view.optionpanel.ShowMessage;

public class Write extends JDialog implements ActionListener{
	
	private static final long serialVersionUID = 1L;
	private JPanel filePanel;
	private JTextField fileNameField;
	private JLabel fileNameLabel,typeNameJLabel,fileNameLabel1;
	private JButton teamLoginButton,modifyButton;
	int id;
	int size = 0,a=0,b=0,size2=0;
	int start;
	
	private int y[]=new int[128],u[]=new int[128];
	
	public Write(int id,int start){
		this.setTitle("写文件");
		this.id=id;
		this.start =start;
		this.setSize(300, 190);
		JPanel jPanel = getTeamPanel();
		this.setContentPane(jPanel);
		this.setLocationRelativeTo(null);
		this.setModal(true);
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setVisible(true);
	}
	

	private JPanel getTeamPanel(){
		filePanel = new JPanel();
		filePanel.setLayout(null);
        InputStream f1 = null;
		try {
			f1 = new FileInputStream("resources/"+this.id+".txt");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        try {
			size = f1.available();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        try {
			f1.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		typeNameJLabel = new JLabel("该文件为  "+size+" Bytes!");
		typeNameJLabel.setBounds(10, 20, 160, 20);
		
		fileNameLabel = new JLabel("将缓存区内容写入到第");
		fileNameLabel.setBounds(10,60, 180, 20);
		
		fileNameField = new JTextField();
		fileNameField.setBounds(155, 60, 40, 20);
		fileNameField.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				int keyChar=e.getKeyChar();
				if (keyChar>=KeyEvent.VK_0 && keyChar<=KeyEvent.VK_9) {
					
				} else {
					e.consume();  
				}
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		fileNameLabel1 = new JLabel("个字节以后！");
		fileNameLabel1.setBounds(200, 60, 150, 20);
		
		


		teamLoginButton = new JButton("确   定");
		teamLoginButton.addActionListener(this);
		teamLoginButton.setBounds(20, 110, 100, 30);
		modifyButton = new JButton("取  消");
		modifyButton.addActionListener(this);
		modifyButton.setBounds(140, 110, 100, 30);
		filePanel.add(fileNameLabel);
		filePanel.add(fileNameField);
		filePanel.add(typeNameJLabel);
		filePanel.add(fileNameLabel1);
		filePanel.add(teamLoginButton);
		filePanel.add(modifyButton);
		return filePanel;			
	}

	@Override
	public void actionPerformed(ActionEvent u) {
		// TODO Auto-generated method stub
		if(u.getActionCommand().equals("确   定")){
			a=Integer.parseInt(fileNameField.getText());
			InputStream f1 = null;
			try {
				f1 = new FileInputStream("resources/test4.txt");
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				b = f1.available();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				f1.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			InputStream f2= null;
			try {
				f2 = new FileInputStream("resources/test3.txt");
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				size2 = f2.available();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				f2.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			try {
				kuaihao();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			System.out.println("start"+start);
			
			if(start!=-128){
	        /* 创建输入和输出流 */
	        FileInputStream fis = null;
	        FileInputStream fis1 = null,fis2 = null,fis3 = null;
	        FileOutputStream fos = null,fos1=null;
	 
	        try {

	            
	            fis1 = new FileInputStream("resources/"+this.id+".txt");
	            fos = new FileOutputStream("resources/test4.txt");
	            
	            byte[] buf = new byte[1024 * 1024];
	            byte[] buf1 = new byte[1024 * 1024];
	            byte[] buf2 = new byte[1024 * 1024];
	            byte[] buf3 = new byte[1024 * 1024];
	            
	            int len;
	            while ((len = fis1.read(buf)) != -1) {
	                fos.write(buf, 0, len);
	            }
	            fis = new FileInputStream("resources/test3.txt");
	            fis2 = new FileInputStream("resources/test4.txt");
	            fos1 = new FileOutputStream("resources/"+this.id+".txt");
	            
	            int len1,len2 = 0;
	            while ((len1 = fis2.read(buf1)) != -1) {
	                fos1.write(buf1, 0, a);
	            }
	            System.out.println(len1);
	            while ((len2 = fis.read(buf2)) != -1) {
	                fos1.write(buf2, 0, len2);
	            }
	            fis3 = new FileInputStream("resources/test4.txt");
	            while ((len1 = fis3.read(buf3)) != -1) {
	                fos1.write(buf3,a, len1-a);
	            }
	            
	            File q = new File("resources/test3.txt");
	    		Writer rdf = null;
	    			rdf = new FileWriter(q);
	    			rdf.close();

	        } catch (FileNotFoundException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        } catch (IOException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        } finally {
	            try {
	                fis.close();
	                fos.close();
	            } catch (IOException e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	            }
	        }
			
			
			dispose();
		}
			else {
				ShowMessage.error("磁盘内存已满！", null);
				dispose();
			}
		}
		
		else if(u.getActionCommand().equals("取  消")){
			dispose();
		}
	}
	
	
	public int getWritePoint(){
		System.out.println("a+b"+a+b);
		return a+b;
	}
	
	private void kuaihao() throws Exception{
		
		
			if(true){
			FileReader fr=null;
			try{
			fr=new FileReader("resources/test2.txt");
			}catch(Exception e){
				System.out.println("第一个catch");
			}
	        BufferedReader br=new BufferedReader(fr);
	        String line="";
	        int i=0;
	        int a[]=new int[128];
	        try {
				while ((line=br.readLine())!=null) {
					int d = Integer.parseInt(line);
					a[i]=d;a[0]=-1;
					System.out.println(a[i]);
					i++;

				}
			} catch (NumberFormatException e) {
				System.out.println("第2个catch");
			} catch (IOException e) {
				System.out.println("第3个catch");
			}
	        try {
				br.close();
				fr.close();
			} catch (IOException e) {
				System.out.println("第4个catch");
			}
	        
	       
	        if(start>0){
	        	int b=start;
	        	while(b!=-1){
	        		int c=a[b];
	        		if(a[b]==-1){
	        			a[b]=0;
	        			break;
	        		}
	        		else{
	        		a[b]=0;
	        		b=c;
	        		System.out.println(b);
	        		}
	        	}
	        }
	        
	        File q = new File("resources/test2.txt");
			Writer rdf = null;
				try {
					rdf = new FileWriter(q);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				try {
					rdf.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        
	        FileWriter fileWriter = null;
			try {
				fileWriter = new FileWriter("resources/test2.txt",true);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			for (int f = 0; f < a.length; f++) {
			try {
				fileWriter.write(String.valueOf(a[f])+"\n");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}
			try {
				fileWriter.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				fileWriter.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			}
		
		
		
		
		
		
		FileReader fr=new FileReader("resources/test2.txt");
        BufferedReader br=new BufferedReader(fr);
        String line="";
        int i=0;
        while ((line=br.readLine())!=null) {
        	int d = Integer.parseInt(line);
        	y[i]=d;y[0]=-1;
        	u[i]=d;u[0]=-1;
        	i++;
//            System.out.println(d);
        }
        br.close();
        fr.close();
        
        int size1=size+size2;
        
        System.out.println("size"+size1);
        int j=1;
        for(;j<128;j++){
        	if(y[j]==0){
        		this.start=j;
//        		System.out.println(j);
        		size1=size1-64;
        		break;
        	}
        }
        
        int k=j+1;
        if(size1>0){
        	if(j<127){
        	for(;k<128;k++){
        		if(y[k]==0){
        			y[j]=k;
        			j=k;
        			size1=size1-64;
        		}
        		 if(size1<=0){
        			 y[k]=-1;
        			 break;
        		 }
        	}
        	if(size1>0){
        		y=u;
        		start=-128;
        	}
        }
        	else {
        		y=u;
        		start=-128;
			}
        }
        else {
			y[j]=-1;
		}
        
        File q = new File("resources/test2.txt");
		Writer rdf = null;
			rdf = new FileWriter(q);
//			rdf.write("-1\n");
			rdf.close();
        
        FileWriter fileWriter=new FileWriter("resources/test2.txt",true);
		for (int f = 0; f < y.length; f++) {
		fileWriter.write(String.valueOf(y[f])+"\n");
		}
		fileWriter.flush();
		fileWriter.close();
	}
}
