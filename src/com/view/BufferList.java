
	package com.view;

	import java.awt.BorderLayout;
	import java.awt.event.ActionEvent;
	import java.awt.event.ActionListener;
	import java.awt.event.MouseAdapter;
	import java.awt.event.MouseEvent;
	import javax.swing.JButton;
	import javax.swing.JFrame;
	import javax.swing.JLabel;
	import javax.swing.JPanel;
	import javax.swing.JScrollPane;
	import javax.swing.JTable;
	import javax.swing.JTextField;
	import javax.swing.ListSelectionModel;
	import javax.swing.table.DefaultTableModel;
import java.awt.Button;
import java.awt.Dimension;

import java.util.List;


import javax.swing.table.DefaultTableCellRenderer;

	import com.control.BufferControl;
	import com.model.Buffer;

	public class BufferList extends JFrame{
		private static final long serialVersionUID = 1L;
		List<Buffer> buffers=BufferControl.getInstance().getBuffers();
		private DefaultTableModel tableModel;
		JTable table;
		
		public BufferList(){
			this.setSize(815, 220);
			this.setLocation(325, 250);
			JPanel jPanel = new JPanel();
				
	        
			String[] columnNames={"文件路径名","文件属性","起始盘块号","文件长度（B）","操作类型","读指针块号","写指针块号"};
			int k=buffers.size();
			
			if(k==0){
				Object[][]data={};
				tableModel=new DefaultTableModel(data,columnNames);
			}
			else if(k==1){
				Buffer a=buffers.get(0);
				Button b=new Button("关闭");
				Object[][]data={{a.getPath(),a.getFileType(),a.getStart(),a.getFileLength(),a.getOType(),a.getReadPoint(),a.getWritePoint()}};
				 tableModel = new DefaultTableModel(data,columnNames);
			}
			else if(k==2){
				Buffer a=buffers.get(0);
				Buffer a1=buffers.get(1);
				Object[][]data={{a.getPath(),a.getFileType(),a.getStart(),a.getFileLength(),a.getOType(),a.getReadPoint(),a.getWritePoint()},
						{a1.getPath(),a1.getFileType(),a1.getStart(),a1.getFileLength(),a1.getOType(),a1.getReadPoint(),a1.getWritePoint()}};
				tableModel = new DefaultTableModel(data,columnNames);
			}
			else if(k==3){
				Buffer a=buffers.get(0);
				Buffer a1=buffers.get(1);
				Buffer a2=buffers.get(2);
				Object[][]data={{a.getPath(),a.getFileType(),a.getStart(),a.getFileLength(),a.getOType(),a.getReadPoint(),a.getWritePoint()},
						{a1.getPath(),a1.getFileType(),a1.getStart(),a1.getFileLength(),a1.getOType(),a1.getReadPoint(),a1.getWritePoint()},
						{a2.getPath(),a2.getFileType(),a2.getStart(),a2.getFileLength(),a2.getOType(),a2.getReadPoint(),a2.getWritePoint()}};
				tableModel = new DefaultTableModel(data,columnNames);
			}
			else if(k==4){
				Buffer a=buffers.get(0);
				Buffer a1=buffers.get(1);
				Buffer a2=buffers.get(2);
				Buffer a3=buffers.get(3);
				Object[][]data={{a.getPath(),a.getFileType(),a.getStart(),a.getFileLength(),a.getOType(),a.getReadPoint(),a.getWritePoint()},
						{a1.getPath(),a1.getFileType(),a1.getStart(),a1.getFileLength(),a1.getOType(),a1.getReadPoint(),a1.getWritePoint()},
						{a2.getPath(),a2.getFileType(),a2.getStart(),a2.getFileLength(),a2.getOType(),a2.getReadPoint(),a2.getWritePoint()},
						{a3.getPath(),a3.getFileType(),a3.getStart(),a3.getFileLength(),a3.getOType(),a3.getReadPoint(),a3.getWritePoint()}};
				tableModel = new DefaultTableModel(data,columnNames);
			}
			else if(k==5){
				Buffer a=buffers.get(0);
				Buffer a1=buffers.get(1);
				Buffer a2=buffers.get(2);
				Buffer a3=buffers.get(3);
				Buffer a4=buffers.get(4);
				Object[][]data={{a.getPath(),a.getFileType(),a.getStart(),a.getFileLength(),a.getOType(),a.getReadPoint(),a.getWritePoint()},
						{a1.getPath(),a1.getFileType(),a1.getStart(),a1.getFileLength(),a1.getOType(),a1.getReadPoint(),a1.getWritePoint()},
						{a2.getPath(),a2.getFileType(),a2.getStart(),a2.getFileLength(),a2.getOType(),a2.getReadPoint(),a2.getWritePoint()},
						{a3.getPath(),a3.getFileType(),a3.getStart(),a3.getFileLength(),a3.getOType(),a3.getReadPoint(),a3.getWritePoint()},
						{a4.getPath(),a4.getFileType(),a4.getStart(),a4.getFileLength(),a4.getOType(),a4.getReadPoint(),a4.getWritePoint()}};
				tableModel = new DefaultTableModel(data,columnNames);
				
			}
			table=new JTable(tableModel);
			
			
			
			 table.getColumnModel().getColumn(0).setPreferredWidth(240);
			 table.getColumnModel().getColumn(1).setPreferredWidth(50);
			 table.getColumnModel().getColumn(2).setPreferredWidth(50);
			 table.getColumnModel().getColumn(3).setPreferredWidth(80);
			 table.getColumnModel().getColumn(4).setPreferredWidth(50);
			 table.getColumnModel().getColumn(5).setPreferredWidth(50);
			 table.getColumnModel().getColumn(6).setPreferredWidth(50);
			 

			 table.setPreferredScrollableViewportSize(new Dimension(800,120));
			 
			 DefaultTableCellRenderer tcr = new DefaultTableCellRenderer();//单元格渲染器  
			 tcr.setHorizontalAlignment(JLabel.CENTER);//居中显示  
			 table.setDefaultRenderer(Object.class, tcr);//设置渲染器  
			 
			 final JButton delButton = new JButton("关闭");
		        delButton.addActionListener(new ActionListener(){//添加事件
		            public void actionPerformed(ActionEvent e){
		                int selectedRow = table.getSelectedRow();//获得选中行的索引
		                if(selectedRow!=-1)  //存在选中行
		                {
		                	buffers.remove(buffers.get(selectedRow));
		                    tableModel.removeRow(selectedRow);  //删除行
		                }
		            }
		        });
		       
			 JScrollPane scrollPane=new JScrollPane(table);
			 getContentPane().add(scrollPane,BorderLayout.CENTER);
			 jPanel.add(scrollPane);
			 getContentPane().add(delButton,BorderLayout.SOUTH);
		        jPanel.add(delButton);
			    this.add(jPanel);
				this.setTitle("已打开文件表");
				this.setVisible(true);
			 }
	}

