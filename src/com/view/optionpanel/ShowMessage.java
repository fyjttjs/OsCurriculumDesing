package com.view.optionpanel;



import javax.swing.JOptionPane;

public class ShowMessage {

	public static void information(String message, String title) {
		String titles = "提示";
		if (check(title))
			titles = title;
		JOptionPane.showMessageDialog(null, message, titles, JOptionPane.INFORMATION_MESSAGE);
	}
	
	public static void question(String message, String title) {
		String titles = "疑问";
		if (check(title))
			titles = title;
		JOptionPane.showMessageDialog(null, message, titles, JOptionPane.QUESTION_MESSAGE);
	}
	
	public static void warning(String message, String title) {
		String titles = "警告";
		if (check(title))
			titles = title;
		JOptionPane.showMessageDialog(null, message, titles, JOptionPane.WARNING_MESSAGE);
	}
	
	public static void error(String message, String title) {
		String titles = "错误";
		if (check(title))
			titles = title;
		JOptionPane.showMessageDialog(null, message, titles, JOptionPane.WARNING_MESSAGE);
	}

	private static Boolean check(String check) {
		if (check == null)
			return false;
		else
			return true;
	}

}
