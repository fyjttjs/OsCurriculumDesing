package com.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;



public class Read extends JDialog implements ActionListener{
	private static final long serialVersionUID = 1L;
	private JPanel filePanel;
	private JTextField fileNameField,fileNameField1;
	private JLabel fileNameLabel,typeNameJLabel,fileNameLabel1,fileNameLabel2,fileNameLabel3;
	private JButton teamLoginButton,modifyButton;
	int id;
	int size = 0;
	int a=0,b=0;
	
	public Read(int id){
		this.setTitle("读文件");
		this.id=id;
		this.setSize(280, 210);
		JPanel jPanel = getTeamPanel();
		this.setContentPane(jPanel);
		this.setLocationRelativeTo(null);
		this.setModal(true);
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setVisible(true);
	}
	
	private JPanel getTeamPanel(){
		filePanel = new JPanel();
		filePanel.setLayout(null);
        InputStream f1 = null;
		try {
			f1 = new FileInputStream("resources/"+this.id+".txt");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        try {
			size = f1.available();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        try {
			f1.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		typeNameJLabel = new JLabel("该文件为  "+size+" Bytes!");
		typeNameJLabel.setBounds(30, 10, 160, 20);
		
		fileNameLabel = new JLabel("从第");
		fileNameLabel.setBounds(30, 40, 80, 20);
		
		fileNameField = new JTextField();
		fileNameField.setBounds(70, 40, 40, 20);
		fileNameField.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				int keyChar=e.getKeyChar();
				if (keyChar>=KeyEvent.VK_0 && keyChar<=KeyEvent.VK_9) {
					
				} else {
					e.consume();  
				}
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		fileNameLabel1 = new JLabel("个字节开始读取！");
		fileNameLabel1.setBounds(120, 40, 150, 20);
		
		fileNameLabel2 = new JLabel("共读取");
		fileNameLabel2.setBounds(30, 70, 80, 20);
		
		fileNameField1 = new JTextField();
		fileNameField1.setBounds(80, 70, 40, 20);
        fileNameField1.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				int keyChar=e.getKeyChar();
				if (keyChar>=KeyEvent.VK_0 && keyChar<=KeyEvent.VK_9) {
					
				} else {
					e.consume();  
				}
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		fileNameLabel3 = new JLabel("个字节！");
		fileNameLabel3.setBounds(130, 70, 80, 20);


		teamLoginButton = new JButton("确   定");
		teamLoginButton.addActionListener(this);
		teamLoginButton.setBounds(20, 120, 100, 30);
		modifyButton = new JButton("取  消");
		modifyButton.addActionListener(this);
		modifyButton.setBounds(140, 120, 100, 30);
		filePanel.add(fileNameLabel);
		filePanel.add(fileNameField);
		filePanel.add(fileNameField1);
		filePanel.add(typeNameJLabel);
		filePanel.add(fileNameLabel1);
		filePanel.add(fileNameLabel2);
		filePanel.add(fileNameLabel3);
		filePanel.add(teamLoginButton);
		filePanel.add(modifyButton);
		return filePanel;			
	}

	@Override
	public void actionPerformed(ActionEvent u) {
		// TODO Auto-generated method stub
		if(u.getActionCommand().equals("确   定")){
			a=Integer.parseInt(fileNameField.getText());
			b=Integer.parseInt(fileNameField1.getText());
			
			
			
	        /* 创建输入和输出流 */
	        FileInputStream fis = null;
	        FileOutputStream fos = null;
	 
	        try {
	            /* 将io流和文件关联 */
	            fis = new FileInputStream("resources/"+this.id+".txt");
	 
	            fos = new FileOutputStream("resources/test3.txt");
	            byte[] buf = new byte[1024 * 1024];
	            int len=0;
	            while ((len = fis.read(buf)) != -1) {
	                fos.write(buf, a-1, b);
	            }

	        } catch (FileNotFoundException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        } catch (IOException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        } finally {
	            try {
	                fis.close();
	                fos.close();
	            } catch (IOException e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	            }
	        }
			
			
			dispose();
		}
		else if(u.getActionCommand().equals("取  消")){
			dispose();
		}
	}
	
	public int getReadPoint(){
		return a+b;
	}
	
}
