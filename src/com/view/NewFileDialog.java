package com.view;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;



public class NewFileDialog extends JDialog implements ActionListener{
	private static final long serialVersionUID = 1L;
	private JPanel filePanel;
	private JComboBox<String> typeNameField;
	private JTextField fileNameField;
	private JLabel fileNameLabel,typeNameJLabel;
	private JButton teamLoginButton,modifyButton;
	int i;
	public NewFileDialog(){
		this.setTitle("新建文件");
		this.setSize(280, 210);
		JPanel jPanel = getTeamPanel();
		this.setContentPane(jPanel);
		this.setLocationRelativeTo(null);
		this.setModal(true);
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setVisible(true);
	}
	
	private JPanel getTeamPanel(){
		filePanel = new JPanel();
		filePanel.setLayout(null);
		typeNameJLabel = new JLabel("文件类型");
		typeNameJLabel.setBounds(30, 70, 80, 30);
		typeNameField = new JComboBox<String>(new String[]{"选择文件类别",".只读",".可读写",".系统"});
		typeNameField.setBounds(110, 70, 120, 30);
		fileNameLabel = new JLabel("文件名字");
		fileNameLabel.setBounds(30, 20, 80, 30);
		fileNameField = new JTextField();
		fileNameField.setBounds(110, 20, 120, 30);
		teamLoginButton = new JButton("确   定");
		teamLoginButton.addActionListener(this);
		teamLoginButton.setBounds(20, 120, 100, 30);
		modifyButton = new JButton("取  消");
		modifyButton.addActionListener(this);
		modifyButton.setBounds(140, 120, 100, 30);
		filePanel.add(fileNameLabel);
		filePanel.add(fileNameField);
		filePanel.add(typeNameJLabel);
		filePanel.add(typeNameField);
		filePanel.add(teamLoginButton);
		filePanel.add(modifyButton);
		return filePanel;			
	}
	
	public String getFileName(){
		return fileNameField.getText().toString();
	}
	
	public String getFileType(){
		return typeNameField.getSelectedItem().toString();
	}
	
	public int get_i() {
		return i;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equals("确   定")){
			i=1;
			dispose();
		}
		else if(e.getActionCommand().equals("取  消")){
			i=0;
			dispose();
		}
		
	}

}
