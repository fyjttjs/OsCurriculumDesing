package com.view;


import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class HelpDialog extends JDialog{

	private static final long serialVersionUID = 1L;

	public HelpDialog(int width,int height){
		this.setSize(300, 150);
		this.setLocation(width+100, height+50);
		JPanel jPanel = new JPanel();
		JLabel help = new JLabel();
		String message = "<html><body><div align=\"left\">"
				+"cd：切换目录，默认为相对路径"
				+"<br/>" 
		        +"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-a 切换到绝对路径"
				+"<hr/>"
		        +"rm：删除文件，默认为当前目录下的文件"
				+"<br/>"
				+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-a 删除绝对目录下的文件"
		        +"</div></body></html>";
		help.setText(message);
		jPanel.add(help);
		this.add(jPanel);
		this.setTitle("命令行帮助");
		this.setVisible(true);
	}

}
