package com.app;

import java.awt.Font;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.UIManager;

import org.jfree.chart.labels.BubbleXYItemLabelGenerator;
import org.jvnet.substance.SubstanceLookAndFeel;
import org.jvnet.substance.button.StandardButtonShaper;
import org.jvnet.substance.painter.SpecularWaveGradientPainter;
import org.jvnet.substance.painter.StandardGradientPainter;
import org.jvnet.substance.painter.noise.WoodFilter;
import org.jvnet.substance.skin.AutumnSkin;
import org.jvnet.substance.skin.OfficeSilver2007Skin;
import org.jvnet.substance.theme.SubstanceOliveTheme;
import org.jvnet.substance.watermark.SubstanceBinaryWatermark;
import org.jvnet.substance.watermark.SubstanceBubblesWatermark;
import org.jvnet.substance.watermark.SubstanceWatermark;

import com.sun.javafx.sg.prism.web.NGWebView;
import com.view.MainFrame;

public class App {

	public static void main(String[] args) {
		JFrame.setDefaultLookAndFeelDecorated(true);
		JDialog.setDefaultLookAndFeelDecorated(true);
		try {
            UIManager.setLookAndFeel(new SubstanceLookAndFeel());
            JFrame.setDefaultLookAndFeelDecorated(true);
            JDialog.setDefaultLookAndFeelDecorated(true);
            SubstanceLookAndFeel.setSkin(new AutumnSkin());
            SubstanceLookAndFeel.setCurrentButtonShaper(new StandardButtonShaper());
            SubstanceLookAndFeel.setCurrentWatermark(new SubstanceBinaryWatermark());
            SubstanceLookAndFeel.setCurrentGradientPainter(new SpecularWaveGradientPainter());
            //SubstanceLookAndFeel.setCurrentTheme(new SubstanceOliveTheme());
        } catch (Exception e) {
            System.err.println("Something went wrong!");
        }
		setUIFont();
		new MainFrame();

	}
	public static void setUIFont()
	{
		Font f = new Font("微软雅黑",Font.PLAIN,14);
		String   names[]={ "Label", "CheckBox", "PopupMenu","MenuItem", "CheckBoxMenuItem",
				"JRadioButtonMenuItem","ComboBox", "Button", "Tree", "ScrollPane",
				"TabbedPane", "EditorPane", "TitledBorder", "Menu", "TextArea",
				"OptionPane", "MenuBar", "ToolBar", "ToggleButton", "ToolTip",
				"ProgressBar", "TableHeader", "Panel", "List", "ColorChooser",
				"PasswordField","TextField", "Table", "Label", "Viewport",
				"RadioButtonMenuItem","RadioButton", "DesktopPane", "InternalFrame"
		}; 
		for (String item : names) {
			 UIManager.put(item+ ".font",f); 
		}
		UIManager.put("RootPane.setupButtonVisible",false);
	}

}
